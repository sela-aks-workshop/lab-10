# Kubernetes Workshop
Lab 10: Configuring Autoscaling

---

## Instructions

### Check current cluster nodes

 - Check the current nodes of your cluster
```
kubectl get nodes
```

### Deploy a Sample App

 - Deploy the application
```
kubectl apply -f https://gitlab.com/sela-aks-workshop/lab-10/raw/master/load-generator.yaml
```

 - Ensure the deployment resource was created
```
kubectl get deployments
```

 - Ensure the application is up and running
```
kubectl get pods
```

 - Ensure the application service was deployed successfully
```
kubectl get svc
```

### Create an HPA resource (Horizontal Pod autoscaling)

 - Create a resource to scales up when CPU exceeds 50% of the allocated container resource
```
kubectl autoscale deployment load-generator --cpu-percent=50 --min=1 --max=5
```

 - Ensure the HPA resource was created successfully (and watch it status)
```
kubectl get hpa -w
```

### Generate load to trigger scaling

 - In a new terminal run the following command to drop into a shell on a new container
```
kubectl run -i --tty --generator=run-pod/v1 load-trigger --image=busybox /bin/sh
```

 - Execute a while loop to continue getting http:///load-generator and generate load (then minimize the terminal)
```
while true; do wget -q -O - http://load-generator; done
```

 - In the previous terminal watch the HPA with the following command
```
watch -n 5 kubectl get hpa
```

 - You can check the running pods as well
```
kubectl get pods
```
 
### Edit HPA configuration

 - Edit the HPA resource to increment the maxReplicas up to 50 and the minReplicas to 3
```
kubectl edit hpa load-generator
```

 - Watch the HPA with to see how the application is scaled
```
watch -n 5 kubectl get hpa
```

 - You can check the running pods as well
```
kubectl get pods
```

### Check current cluster nodes

 - Watch the current nodes of your cluster to track the cluster autoscaling:
```
watch -n 5 kubectl get nodes
```

### Stop the load to stop the scaling process

 - Stop (Ctrl + C) the load test that was running in the other terminal. 
 
 - You will notice that HPA will slowly bring the replica count to min number based on its configuration

 
### Cleanup

 - List existent resources
```
kubectl get all
```

 - Delete existent resources
```
kubectl delete all --all
```

 - List existent resources
```
kubectl get all
```
